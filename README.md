version -> v8.5

wrapper.py
============
$ python wrapper.py -h
usage: wrapper.py [-h] [--dry_run] --tumorR1FastqFile TUMORR1FASTQFILE
                  [--normalR1FastqFile NORMALR1FASTQFILE] --outDir OUTDIR
                  --cores CORES --sampleName SAMPLENAME --outputLogFile
                  OUTPUTLOGFILE [--dbnsfp DBNSFP] [--gnomad GNOMAD]
                  [--normalreferencepool NORMALREFERENCEPOOL]
                  [--samplebio SAMPLEBIO] [--extbases EXTBASES]

command line arguments for calling cce variant caller pipeline

optional arguments:
  -h, --help            show this help message and exit
  --dry_run             perform all the actions without calling pipeline steps
  --tumorR1FastqFile TUMORR1FASTQFILE
                        tumor sample R1 fastq file
  --normalR1FastqFile NORMALR1FASTQFILE
                        normal sample R1 fastq file; for tumor only analysis
                        set this parameter to NA
  --outDir OUTDIR       output folder. subfolders will be created in this
                        folder
  --cores CORES         maximum cpu cores
  --sampleName SAMPLENAME
                        sample name
  --outputLogFile OUTPUTLOGFILE
                        output log file
  --dbnsfp DBNSFP       dbNSFP file
  --gnomad GNOMAD       gnomad vcf file
  --normalreferencepool NORMALREFERENCEPOOL
                        normal reference pool file for cnvkit
  --samplebio SAMPLEBIO
                        samplebio file
  --extbases EXTBASES   extend bedfile regions on both ends (bases)

