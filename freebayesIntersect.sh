# README
# This script does:
# intersect calls from freebayes normal and tumor normal calls


# Input paramters, required:
tnVcf=$1
nVcf=$2
outvcf=$3
outpath=$4

OUT=$outpath/freebayes

## run bedtools intersect
bedtools intersect -wa -a $tnVcf -b $nVcf > $OUT/$outvcf
grep "^#" $tnVcf > $OUT/header
cat $OUT/header  $OUT/$outvcf >  $OUT/$outvcf\.2
rm $OUT/$outvcf
mv $OUT/$outvcf\.2 $OUT/$outvcf

