#!/usr/bin/python

import os
import sys
import argparse
import string
import subprocess
#import csv
#import _mssql
import re
import json
#import requests
#import socket
#import platform
from datetime import datetime
#from operator import itemgetter
#from BeautifulSoup import BeautifulSoup
import shutil
#from shutil import copyfile
#from time import gmtime, strftime
#from distutils.dir_util import copy_tree

def main():

        ## command line arguments
        parser = argparse.ArgumentParser(description = 'command line arguments for calling cce variant caller pipeline')
        parser.add_argument('--dry_run', dest='dry_run', required=False, action='store_true', help='perform all the actions without calling pipeline steps')
	parser.add_argument('--tumorR1FastqFile', type=str, dest='tumorR1FastqFile', required=True, help='tumor sample R1 fastq file')
	parser.add_argument('--normalR1FastqFile', type=str, dest='normalR1FastqFile', required=False, help='normal sample R1 fastq file; for tumor only analysis set this parameter to NA')
	parser.add_argument('--outDir', type=str, dest='outDir', required=True, help='output folder. subfolders will be created in this folder')
	parser.add_argument('--cores', type=int, dest='cores', required=True, help='maximum cpu cores')
	parser.add_argument('--sampleName', type=str, dest='sampleName', required=True, help='sample name')
        parser.add_argument('--outputLogFile', type=str, dest='outputLogFile', required=True, help='output log file')
	parser.add_argument('--dbnsfp', type=str, dest='dbnsfp', required=False, help='dbNSFP file')
	parser.add_argument('--gnomad', type=str, dest='gnomad', required=False, help='gnomad vcf file')
	parser.add_argument('--normalreferencepool', type=str, dest='normalreferencepool', required=False, help='normal reference pool file for cnvkit')
	parser.add_argument('--samplebio', type=str, dest='samplebio', required=False, help='samplebio file')
	parser.add_argument('--extbases', type=int, dest='extbases', required=False, help='extend bedfile regions on both ends (bases)')
	
        ## command line argument object
        args = parser.parse_args()

        ## open log file in append mode
        logFile = open (args.outputLogFile,'w')
	
	## pipeline path and current working directory
	pipelinePath = os.path.dirname(os.path.abspath(__file__))
	cwd = os.getcwd()

	## get R2 fasta files
	(tumorR2FastqFile,normalR2FastqFile) = getR2FastqFiles(args.tumorR1FastqFile, args.normalR1FastqFile, logFile)
	
	## read samplebio file to check for tumor cell percentage
	tcp = "NA"
	if args.samplebio:
		sbfh = open (os.path.join(args.samplebio), 'r')
		for line in sbfh.readline():
			ltemp = line.rstrip().split("\t")
			if ltemp[0] == "Tumourcellularity":
				tcp = float(ltemp[1])/100
		sbfh.close()
	
	## check on bedfile extend bases input
	extensionBases = None
	if args.extbases:
		extensionBases = int(args.extbases)
	
	## validate inputs
	if (validateArguments(args,tumorR2FastqFile,normalR2FastqFile,logFile)):
		logme(logFile, "All arguments seem to be ok. Moving forward.", "MESSAGE")
		
		## sample tumor normal names and running mode
		runningPairMode = False
		sampleTumor = "NA"
		sampleNormal = "NA"
		
		## get bed file and ref file from singularity img
		refFile = getRefFile(logFile)
		bedFile = getBedFile(logFile)
		(dbsnp, cosmic, clinvar, mvl1snps, mvl1indels, mvl2snps, mvl2indels) = getAnnotationFiles(logFile)
		
		## create folder with sample name in out dir
		outfolder = createOutputFolder(args, logFile) ## need to fix bug on existing folder rename
		jobLogFile = os.path.join(outfolder, args.sampleName) + ".log"
	
		## run fastq trim for tumors
		sampleTumor = args.sampleName + ".tumor"
		cmd = ["bash", str(pipelinePath) + "/fastqtrim.sh" , str(args.cores) ,  args.tumorR1FastqFile , tumorR2FastqFile, str(outfolder) , sampleTumor, "&>>", jobLogFile]
		if (not(args.dry_run)):
			runCommand(cmd, logFile)
	
		## run fastq trim for normals
		if args.normalR1FastqFile != "NA":
			sampleNormal = args.sampleName + ".normal"
			runningPairMode = True
			cmd = ["bash", str(pipelinePath) + "/fastqtrim.sh" , str(args.cores), args.normalR1FastqFile, normalR2FastqFile, str(outfolder) , sampleNormal, "&>>", jobLogFile ]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
		
		## run bwa for tumors
		(fqR1,fqR2) = ("NA", "NA")
		for root, dirs, files in os.walk(os.path.join(outfolder,"fastqTrim")):
			for fq in files:
				if fq.startswith(sampleTumor) and fq.endswith("R1.fastq.gz"):
					fqR1 = os.path.join(root, fq)
				if fq.startswith(sampleTumor) and fq.endswith("R2.fastq.gz"):
					fqR2 = os.path.join(root, fq)
		cmd = ["bash" , str(pipelinePath) + "/bwa.sh" ,  str(args.cores) , fqR1 , fqR2 , str(outfolder) , sampleTumor , refFile, bedFile ,"&>>" , jobLogFile]
		if (not(args.dry_run)):
			runCommand(cmd, logFile)
		
		## run bwa for normals
		if args.normalR1FastqFile != "NA":
			for root, dirs, files in os.walk(os.path.join(outfolder,"fastqTrim")):
                        	for fq in files:
                                	if fq.startswith(sampleNormal) and fq.endswith("R1.fastq.gz"):
                                        	fqR1 = os.path.join(root, fq)
                                	if fq.startswith(sampleNormal) and fq.endswith("R2.fastq.gz"):
                                        	fqR2 = os.path.join(root, fq)
			cmd = ["bash" , str(pipelinePath) + "/bwa.sh" ,  str(args.cores) , fqR1 , fqR2, str(outfolder) , sampleNormal , refFile, bedFile ,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
		
		
		## tumor and normal bam files
		tumorBam = "NA"
		normalBam = "NA"
		for root, dirs, files in os.walk(os.path.join(outfolder,"bams")):
			for bam in files:
				if bam.startswith(sampleTumor) and bam.endswith(".bam"):
					tumorBam = os.path.join(root, bam)
				if bam.startswith(sampleNormal) and bam.endswith(".bam"):
					normalBam = os.path.join(root, bam)
		
		
		## run the bamQC R script
		tumorMeanCovFile = "NA"
		normalMeanCovFile = "NA"
		outBamQCPath = os.path.join(outfolder,"bamQC")
		for root, dirs, files in os.walk(os.path.join(outfolder,"bamQC")):
			for meanFile in files:
				if meanFile.endswith("tumor.mean.txt"):
					tumorMeanCovFile = os.path.join(root, meanFile)
				if meanFile.endswith("normal.mean.txt"):
					normalMeanCovFile = os.path.join(root, meanFile)
		if tumorMeanCovFile != "NA":
			cmd = ["Rscript" , str(pipelinePath) + "/bamqc.R" , tumorMeanCovFile , normalMeanCovFile, str(outBamQCPath)]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)			
		
		## extend bed file
                if extensionBases and extensionBases > 0:
                        (newbedfile,newbedfileflat) = extendBedfile(outfolder, bedFile, extensionBases, logFile)
			## coverage on tumor and normal bam file on the new extended bedfile regions
			outpathbamqc = os.path.join(outfolder,"bamQC")
			samplename = args.sampleName + ".tumor"
			cmd = ["bash" , str(pipelinePath) + "/coverageExtendedBed.sh" ,  tumorBam , outpathbamqc , samplename , newbedfileflat ,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
			samplename = args.sampleName + ".normal"
			cmd = ["bash" , str(pipelinePath) + "/coverageExtendedBed.sh" ,  normalBam , outpathbamqc , samplename , newbedfileflat ,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
                                runCommand(cmd, logFile)
		
		## run manta
		if runningPairMode:
			cmd = ["bash" , str(pipelinePath) + "/manta.sh" ,  str(args.cores) , tumorBam , normalBam , refFile , str(outfolder) ,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
				
		## run strelka
		if runningPairMode:
			bedfileforjob = bedFile
			if os.path.exists(newbedfile):
				bedfileforjob = newbedfile
			cmd = ["bash" , str(pipelinePath) + "/strelka.sh" ,  str(args.cores) , tumorBam , normalBam , refFile , str(outfolder) , bedfileforjob ,  "&>>" , jobLogFile]
                	if (not(args.dry_run)):
				runCommand(cmd, logFile)	
		
		## run freebayes if normal sample is given
		if normalBam != "NA":
			bedfileforjob = bedFile
                        if os.path.exists(newbedfile):
                                bedfileforjob = newbedfile
			outVcf = args.sampleName + ".freebayes.vcf"
			cmd = ["bash" , str(pipelinePath) + "/freebayes.sh" , normalBam , refFile , bedfileforjob, outVcf, outfolder,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
	
		## run freebayes on tumor sample
		if tumorBam != "NA":
			bedfileforjob = bedFile
                        if os.path.exists(newbedfile):
                                bedfileforjob = newbedfile
			outVcf = args.sampleName + ".tumor.freebayes.vcf"
			cmd = ["bash" , str(pipelinePath) + "/freebayes.sh" , tumorBam, refFile , bedfileforjob, outVcf, outfolder,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
		
		## run freebayes on tumor and normal sample together
		if normalBam != "NA" and tumorBam != "NA":
			bedfileforjob = bedFile
                        if os.path.exists(newbedfile):
                                bedfileforjob = newbedfile
			outVcf = args.sampleName + "tumor.normal.freebayes.vcf"
			cmd = ["bash" , str(pipelinePath) + "/freebayes.sh" , str(tumorBam) + " " + str(normalBam), refFile , bedfileforjob, outVcf, outfolder,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
		
		## run pisces on tumor bam file
		if tumorBam != "NA":
			bedfileforjob = bedFile
                        if os.path.exists(newbedfile):
                                bedfileforjob = newbedfile
			refPath = os.path.dirname(refFile)
			cmd = ["bash" , str(pipelinePath) + "/pisces.sh" , bedfileforjob, refPath, tumorBam, outfolder,  "&>>" , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
		
		## run annotation
		if runningPairMode:
			for root, dirs, files in os.walk(os.path.join(outfolder,"manta","results","variants")):
				for vcf in files:
					if vcf.endswith("vcf.gz"):
						fileToAnnotate = os.path.join(root, vcf)
						outputVcf = os.path.join(root, vcf[:-7])
						cmd = ["bash" , str(pipelinePath) + "/annotate.sh" ,  fileToAnnotate , outputVcf , dbsnp, cosmic, clinvar, args.gnomad, args.dbnsfp, "MANTA" ,  "&>>" , jobLogFile]
						if (not(args.dry_run)):
							runCommand(cmd, logFile)
			for root, dirs, files in os.walk(os.path.join(outfolder,"strelka","results","variants")):
                                for vcf in files:
                                        if vcf.endswith("snvindel.targetbed.vcf.gz"):
                                                fileToAnnotate = os.path.join(root, vcf)
                                                outputVcf = os.path.join(root, vcf[:-7])
                                                cmd = ["bash" , str(pipelinePath) + "/annotate.sh" ,  fileToAnnotate , outputVcf , dbsnp, cosmic, clinvar, args.gnomad, args.dbnsfp,  "STRELKA",  "&>>" , jobLogFile]
                                                if (not(args.dry_run)):
                                                        runCommand(cmd, logFile)
		
		for root, dirs, files in os.walk(os.path.join(outfolder,"freebayes")):
			for vcf in files:
				if vcf.endswith("freebayes.vcf"):
					fileToAnnotate = os.path.join(root, vcf)
					outputVcf = os.path.join(root, vcf[:-4])
					cmd = ["bash" , str(pipelinePath) + "/annotate.sh" ,  fileToAnnotate , outputVcf , dbsnp, cosmic, clinvar, args.gnomad, args.dbnsfp, "FREEBAYES", "&>>" , jobLogFile]
					if (not(args.dry_run)):
						runCommand(cmd, logFile)
		
		for root, dirs, files in os.walk(os.path.join(outfolder,"pisces")):
			for vcf in files:
				if vcf.endswith(".vcf"):
					fileToAnnotate = os.path.join(root, vcf)
					outputVcf = os.path.join(root, vcf[:-4])
					cmd = ["bash" , str(pipelinePath) + "/annotate.sh" ,  fileToAnnotate , outputVcf , dbsnp, cosmic, clinvar, args.gnomad, args.dbnsfp, "PISCES", "&>>" , jobLogFile]
					if (not(args.dry_run)):
						runCommand(cmd, logFile)
		
		## run mvl filtration
		outfolders = ["manta/results/variants","strelka/results/variants","freebayes", "pisces"]
		for ofs in outfolders:
			for root, dirs, files in os.walk(os.path.join(outfolder, ofs)):
				for vcf in files:
					if vcf.endswith("ExAC.vcf"):
						inVcf = os.path.join(root,vcf)
						outVcf = os.path.join(root, vcf[:-4]) + ".mvl.vcf"
						cmd = ["perl", str(pipelinePath) + "/filterAgainstMVLs.pl" , mvl1snps, mvl1indels, mvl2snps, mvl2indels, inVcf, outVcf, bedFile]
						if (not(args.dry_run)):
							runCommand(cmd, logFile)
		
		
		## run bedtools intersect on freebayes normal calls and tumor normal calls
		freebayesTumorNormalVcfFile = args.sampleName + "tumor.normal.freebayes.dbSNP.cosmic.clinvar.eff.dbnsfp.passfilter.ExAC.mvl.vcf"
		freebayesNormalVcfFile = args.sampleName + ".freebayes.dbSNP.cosmic.clinvar.eff.dbnsfp.passfilter.ExAC.mvl.vcf"
		vcfPaths = os.path.join(outfolder, "freebayes")
		outvcf = args.sampleName + "tumor.normal.freebayes.dbSNP.cosmic.clinvar.eff.dbnsfp.passfilter.ExAC.mvl.intersectWithFreebayesNormal.vcf" 
		if (os.path.exists(os.path.join(vcfPaths, freebayesTumorNormalVcfFile)) and os.path.exists(os.path.join(vcfPaths, freebayesNormalVcfFile))):
			cmd = ["bash", str(pipelinePath) + "/freebayesIntersect.sh",str(os.path.join(vcfPaths, freebayesTumorNormalVcfFile)), str(os.path.join(vcfPaths, freebayesNormalVcfFile)), outvcf, outfolder,"&>>",jobLogFile ]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)	
		
		## run cnvkit on tumor bam
		if args.normalreferencepool and tumorBam != "NA":
			ballelevcf = os.path.join(outfolder,"freebayes", args.sampleName + "tumor.normal.freebayes.dbSNP.cosmic.clinvar.eff.dbnsfp.passfilter.ExAC.mvl.intersectWithFreebayesNormal.vcf")
			#ballelevcf = args.sampleName + ".freebayes.dbSNP.cosmic.clinvar.eff.passfilter.ExAC.vcf"
			cnvkitSampleName = args.sampleName #os.path.basename(tumorBam)[:-4]
			cmd = ["bash" , str(pipelinePath) + "/cnvkit.sh", tumorBam, str(args.cores), outfolder, cnvkitSampleName, args.normalreferencepool, ballelevcf, tcp, " &>> " , jobLogFile]
			if (not(args.dry_run)):
				runCommand(cmd, logFile)
			
		
		## clean up 
		## remove fastq trim folder
		fqtrimfolder = os.path.join(outfolder,"fastqTrim")
		if os.path.exists(fqtrimfolder) and os.path.isdir(fqtrimfolder):
			shutil.rmtree(fqtrimfolder)
			logme(logFile, "Removed folder " + str(fqtrimfolder), "MESSAGE")
		## zip up manta and strelka workspace folders
		toolnames = ["manta", "strelka"]
		for tf in toolnames:
			tfol =  os.path.join(outfolder, tf)
			wfol = os.path.join(outfolder, tf, "workspace")
			if os.path.exists(wfol) and os.path.isdir(wfol):
				shutil.make_archive( os.path.join(outfolder, tf, "workspace"), 'zip', wfol)
				if (os.path.exists( os.path.join(outfolder, tf, "workspace.zip") )):
					## zip created; now del the workspace folder
					shutil.rmtree(wfol)
					logme(logFile, "Zipped folder " + str(wfol), "MESSAGE")
			
		
	## open log file
	logFile.close()
	


def runCommand(cmd, logFile):
	
	logme(logFile, "Running " + " ".join(cmd), "MESSAGE")
	exit_status_code  = subprocess.call(cmd)  #os.system(cmd)
	if exit_status_code == 0:
		logme(logFile, " ".join(cmd) + " command ended with status code " + str(exit_status_code), "MESSAGE")
	else:
		logme(logFile," ".join(cmd) + " ended with status code " + str(exit_status_code)  + "","ERROR")
		sys.exit(1)




def extendBedfile(outfolder, bedFile, extensionBases, logFile):

        bedfilename = os.path.basename(bedFile)
        newbedfilename = bedfilename[:-3] + "extended." + str(extensionBases) + ".bed"
        newbedfile = os.path.join(outfolder, newbedfilename)
        logme (logFile, "Extending bedfile " + str(bedFile) + " by " + str(extensionBases)+ "bases", "MESSAGE")
        nbh = open (newbedfile, 'w')
        bfh = open (os.path.join(bedFile), 'r')
        for line in bfh.readlines():
                d = line.rstrip().split("\t")
                newstart = int(d[1]) - extensionBases
                newend = int(d[2]) + extensionBases
                oldrange = str(d[0]) + ":" + str(d[1]) + "-" + str(d[2])
                d[1] = str(newstart)
                d[2] = str(newend)
                d[3] = oldrange
                nbh.write("\t".join(d) + "\n")
        bfh.close()
        nbh.close()
	logme (logFile, "New bedfile " + str(newbedfile) , "MESSAGE")
	
	## merge the regions in the new bed file
	newbedfilemerged = str(newbedfile)[:-3] + "merged.bed"
	## bedtools merge -i CMDL_130217_capture_targets_noChr.extended.100.bed > CMDL_130217_capture_targets_noChr.extended.100.merged.bed
	cmd = ["/scif/apps/bedtools/bedtools2/bin/bedtools", "merge", "-i", str(newbedfile), ">",newbedfilemerged]
	os.system(" ".join(cmd))
	#runCommand(cmd, logFile)

        return newbedfilemerged,newbedfile


def getCNVkitFiles(lfile):
	
	af = ""
	rf = ""
	if (os.path.exists("/scif/apps/refgenomes/rubayte-cce_files-68dcb4892505/access.hg19.bed")):
		af = "/scif/apps/refgenomes/rubayte-cce_files-68dcb4892505/access.hg19.bed"
		logme(lfile, "access file for cnvkit found " + af, "MESSAGE")
	else:
		af = "NA"
		logme(lfile, "access file for cnvkit not found ", "ERROR")
		sys.exit(1)
	
	if (os.path.exists("/scif/apps/refgenomes/rubayte-cce_files-68dcb4892505/hg19.refFlat_noChr.txt")):
                rf = "/scif/apps/refgenomes/rubayte-cce_files-68dcb4892505/hg19.refFlat_noChr.txt"
                logme(lfile, "refflat file for cnvkit found " + rf, "MESSAGE")
        else:
                rf = "NA"
                logme(lfile, "refflat file for cnvkit not found ", "ERROR")
                sys.exit(1)
	
	return af,rf



def checkFileExists(fl, lfile):
	
	if (os.path.exists(fl)):
		logme (lfile, "file found " + fl + "", "MESSAGE")
	else:
		logme(lfile,"file not found " + fl + "", "ERROR")
		sys.exit(1)



def getAnnotationFiles(lfile):
	
	dbsnp = ""
	cosmic = ""
	clinvar = ""
	mvl1snps = ""
	mvl1indels = ""
	mvl2snps = ""
	mvl2indels = ""
		
	containerRefGenomePath = "/scif/apps/refgenomes/refGenomeLatest/"
	for root, dirs, files in os.walk(containerRefGenomePath):
		for rfile in files:
			if rfile == "MVL1.sorted.vcf":
				mvl1snps = os.path.join(root, rfile)
				checkFileExists(mvl1snps, lfile)
			elif rfile == "MVL2.sorted.vcf":
				mvl2snps = os.path.join(root, rfile)
				checkFileExists(mvl2snps, lfile)
			elif rfile == "MVL1.rest.tsv":
				mvl1indels = os.path.join(root, rfile)
				checkFileExists(mvl1indels, lfile)
			elif rfile == "MVL2.rest.tsv":
				mvl2indels = os.path.join(root, rfile)
				checkFileExists(mvl2indels, lfile)
			elif rfile == "CosmicCodingMuts.vcf.gz":
				cosmic = os.path.join(root, rfile)
				checkFileExists(cosmic, lfile)
			else:
				continue
	
	containerSnpeffDatabasePath = "/scif/apps/snpeff/snpEff/databases/"
	for root, dirs, files in os.walk(containerSnpeffDatabasePath):
		for sfile in files:
			if sfile.startswith("All_") and sfile.endswith("vcf.gz"):
				dbsnp = os.path.join(root, sfile)
				checkFileExists(dbsnp, lfile)
			elif sfile.startswith("clinvar_") and sfile.endswith("vcf.gz"):
				clinvar = os.path.join(root, sfile)
				checkFileExists(clinvar, lfile)
			else:
				continue
	
	return dbsnp,cosmic,clinvar,mvl1snps,mvl1indels,mvl2snps,mvl2indels

	

def getRefFile(lfile):
	
	rFile = ""
	if (os.path.exists("/scif/apps/refgenomes/refGenomeLatest//hs37d5_phiX174.fa")):
		rFile = "/scif/apps/refgenomes/refGenomeLatest/hs37d5_phiX174.fa"
		logme(lfile, "reference file found " + rFile, "MESSAGE")
	else:
		rFile = "NA"
		logme(lfile, "reference file not found ", "ERROR")
		sys.exit(1)
	return rFile
	


def getBedFile(lfile):
	
	bFile = ""
	if (os.path.exists("/scif/apps/refgenomes/refGenomeLatest/CMDL_130217_capture_targets_noChr.bed")):
		bFile = "/scif/apps/refgenomes/refGenomeLatest/CMDL_130217_capture_targets_noChr.bed"
		logme(lfile, "bed file found " + bFile, "MESSAGE")
	else:
		bFile = "NA"
		logme(lfile, "bed file not found ", "ERROR")
		sys.exit(1)
	return bFile



def createOutputFolder(args,lfile):
	
	## create base output folder with sample name
	baseOutDir = args.outDir + "/" + args.sampleName
	if(os.path.exists(baseOutDir)):
		logme (lfile,"Output folder " + baseOutDir + " exists. Check all things again before running this sample!", "ERROR")
		sys.exit(1)
	else:	
		os.makedirs(baseOutDir)
		logme (lfile,"Output folder " + baseOutDir + " created", "MESSAGE")
	
	return baseOutDir


def getR2FastqFiles(t1file,n1file,lfile):
	
	t2file = t1file.replace("R1", "R2")
	logme(lfile, "Assuming tumor R2 fastq file is " + t2file, "MESSAGE")
	n2file = "NA"
	if (n1file != "NA"):
		n2file = n1file.replace("R1", "R2")
		logme(lfile, "Assuming normal R2 fastq file is " + n2file, "MESSAGE")
	
	return(t2file,n2file)
	

def validateArguments(args,tumorR2File,normalR2File,lfile):
	
	response = False

	## check on input fastq files
	if (os.path.exists(args.tumorR1FastqFile) and os.path.exists(tumorR2File)):
		response = True
	else:
		logme(lfile, "Tumor fastq files not found", "ERROR")
	if (args.normalR1FastqFile is not None): 
		if (os.path.exists(args.normalR1FastqFile) and os.path.exists(normalR2File)):
			response = True
		else:
			logme(lfile, "Normal fastq files not found", "ERROR")
	
	## check on output folder and write permissions
	if (os.path.isdir(args.outDir) and os.access(args.outDir, os.W_OK)):
		response = True
	else:
		logme(lfile, "Output directory not ok", "ERROR")
	
	return response



def logme(lfile,msg,msgType):
	
	lfile.write(str(datetime.now())[:-4] + ":" + msgType + ": " + msg + "\n")

#############
# call main
#############
main()
