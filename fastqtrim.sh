# README
# This script does:
# QC with fastqc and multiqc
# trimming with SeqPurge using defaults
# QC with fastqc and multiqc
# intelligent trim and QC


# Input parameter, required:
cores=$1
fastqR1=$2
fastqR2=$3
outpath=$4
sample=$5

## create outpaths
OUT1=$outpath/fastqQC1
OUTTR=$outpath/fastqTrim
OUT2=$outpath/fastqQC2
OUT3=$outpath/fastqQC3

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

## for intelligent trim
min1adapt=GATCGGAAGAGCACACGTCT
min1adaptreverse=GATCGGAAGAGCGTCGTGTA

if [ ! -d "$OUT1" ]; then
        mkdir $OUT1
fi
if [ ! -d "$OUTTR" ]; then
        mkdir $OUTTR
fi
if [ ! -d "$OUT2" ]; then
        mkdir $OUT2
fi
if [ ! -d "$OUT3" ]; then
        mkdir $OUT3
fi

# first round QC
fastqc --threads $cores --outdir $OUT1 $fastqR1
fastqc --threads $cores --outdir $OUT1 $fastqR2
multiqc --interactive --outdir $OUT1  $OUT1/

# trim all fastq files, forward and reverse reads
SeqPurge -threads $cores -in1 $2 -in2 $3 -out1 $OUTTR/$sample.trim.R1.fastq.gz -out2 $OUTTR/$sample.trim.R2.fastq.gz

# second round QC
fastqc --threads $cores --outdir $OUT2 $OUTTR/$sample.trim.R1.fastq.gz
fastqc --threads $cores --outdir $OUT2 $OUTTR/$sample.trim.R2.fastq.gz
multiqc --interactive --outdir $OUT2 $OUT2/


# check for over represented seq in R1 and R2
OVSEQ1="$(unzip -c $OUT2/$sample.trim.R1_fastqc.zip $sample.trim.R1_fastqc/fastqc_data.txt | awk 'f{print;f=0} /#Sequence\s+Count\s+Percentage\s+Possible\s+Source/{f=1}' |  awk '{print $1;}')" # first overrepresented sequence in R1
OVSEQ2="$(unzip -c $OUT2/$sample.trim.R2_fastqc.zip $sample.trim.R2_fastqc/fastqc_data.txt | awk 'f{print;f=0} /#Sequence\s+Count\s+Percentage\s+Possible\s+Source/{f=1}' |  awk '{print $1;}')" # fírst overrepresented sequence in R2

echo $OVSEQ1
echo $OVSEQ2

# perform intelligent trim
if [[ $OVSEQ1 =~  ^$min1adapt  ]] || [[ $OVSEQ2 =~  ^$min1adaptreverse ]]; then
	SeqPurge -threads $cores -a1 $min1adapt -a2 $min1adaptreverse -in1 $OUTTR/$sample.trim.R1.fastq.gz -in2 $OUTTR/$sample.trim.R2.fastq.gz -out1 $OUTTR/$sample.trim2.R1.fastq.gz -out2 $OUTTR/$sample.trim2.R2.fastq.gz
	mv $OUTTR/$sample.trim.R1.fastq.gz $OUTTR/$sample.trim1.R1.fastq.gz
	mv $OUTTR/$sample.trim.R2.fastq.gz $OUTTR/$sample.trim1.R2.fastq.gz
	mv $OUTTR/$sample.trim2.R1.fastq.gz $OUTTR/$sample.trim.R1.fastq.gz
	mv $OUTTR/$sample.trim2.R2.fastq.gz $OUTTR/$sample.trim.R2.fastq.gz

	# third round QC
	fastqc --threads $cores --outdir $OUT3 $OUTTR/$sample.trim.R1.fastq.gz
	fastqc --threads $cores --outdir $OUT3 $OUTTR/$sample.trim.R2.fastq.gz
	multiqc --interactive --outdir $OUT3 $OUT3/
fi

