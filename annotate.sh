# README run annotation on vcf file

# Input paramters, required:
vcf=$1
outvcf=$2
dbsnp=$3
cosmic=$4
clinvar=$5
gnomad=$6
dbnsfp=$7
filter=$8

## dbsnp annotation
java -jar /scif/apps/snpeff/snpEff/SnpSift.jar Annotate $dbsnp  $vcf  > $outvcf.dbSNP.vcf
## cosmic annotation
java -jar /scif/apps/snpeff/snpEff/SnpSift.jar Annotate $cosmic $outvcf.dbSNP.vcf  > $outvcf.dbSNP.cosmic.vcf
## clinvar annotation
java -jar /scif/apps/snpeff/snpEff/SnpSift.jar Annotate $clinvar $outvcf.dbSNP.cosmic.vcf  > $outvcf.dbSNP.cosmic.clinvar.vcf
## snpeff annotation
java -jar /scif/apps/snpeff/snpEff/snpEff.jar -s $outvcf.dbSNP.cosmic.clinvar.eff.html GRCh37.75 $outvcf.dbSNP.cosmic.clinvar.vcf  > $outvcf.dbSNP.cosmic.clinvar.eff.vcf
## sort the vcf
grep '^#' $outvcf.dbSNP.cosmic.clinvar.eff.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.vcf.header
bedtools sort -i $outvcf.dbSNP.cosmic.clinvar.eff.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.sorted.vcf
cat $outvcf.dbSNP.cosmic.clinvar.eff.vcf.header $outvcf.dbSNP.cosmic.clinvar.eff.sorted.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.sorted.header.vcf
mv $outvcf.dbSNP.cosmic.clinvar.eff.sorted.header.vcf $outvcf.dbSNP.cosmic.clinvar.eff.vcf
## dbnsfp annotation
java -jar /scif/apps/snpeff/snpEff/SnpSift.jar DbNsfp -f SIFT_score,Polyphen2_HDIV_score,Polyphen2_HVAR_score,CADD_phred,FATHMM_score -db $dbnsfp -g hg19 $outvcf.dbSNP.cosmic.clinvar.eff.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.vcf

## cleanup
rm $outvcf.dbSNP.cosmic.clinvar.eff.vcf.header $outvcf.dbSNP.cosmic.clinvar.eff.sorted.vcf 

## filter if caller is strelka
if [ $8 == "STRELKA" ];then
	
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter -r 'LowEVS' "(FILTER = 'LowEVS')| (FILTER = 'PASS') | (FILTER = 'LowDepth')" $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.vcf |  java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter -r 'PASS' "(FILTER = 'LowEVS')| (FILTER = 'PASS') | (FILTER = 'LowDepth')" | java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter -r 'LowDepth' "(FILTER = 'LowEVS')| (FILTER = 'PASS') | (FILTER = 'LowDepth')"  >  $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.vcf
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter -a PASS "(SomaticEVS >= 8.86)" $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.somaticevs.vcf 
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter "(FILTER = 'PASS')" $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.somaticevs.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.somaticevs.filter.vcf
	
	## gnomad vcf annotation
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar Annotate -info AF_popmax,controls_AF_popmax,vep $gnomad $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.somaticevs.filter.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.somaticevs.filter.ExAC.vcf
	
fi;

## filter if caller is manta
if [ $8 == "MANTA" ];then
	
	## gnomad vcf annotation
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar Annotate -info AF_popmax,controls_AF_popmax,vep $gnomad $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.ExAC.vcf
	
fi;

## filter if caller is freebayes
if [ $8 == "FREEBAYES" ];then
	
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter -a PASS "( QUAL >= 22.989 )" $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.vcf
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter "(FILTER = 'PASS')" $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.passfilter.vcf
	
	## gnomad vcf annotation
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar Annotate -info AF_popmax,controls_AF_popmax,vep $gnomad $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.passfilter.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.passfilter.ExAC.vcf
	
fi;

# filter if caller is pisces
if [ $8 == "PISCES" ];then

	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar filter "(FILTER = 'PASS')" $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.vcf
	
	## gnomad vcf annotation
	java -jar /scif/apps/snpeff/snpEff/SnpSift.jar Annotate -info AF_popmax,controls_AF_popmax,vep $gnomad $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.vcf > $outvcf.dbSNP.cosmic.clinvar.eff.dbnsfp.pass.ExAC.vcf


fi;



#SnpSift.jar Annotate [options] database.vcf file.vcf > newFile.vcf
#bedfile=$1


#for sample in *.vcf.gz
#do
#samplebase=$(basename "$sample" .vcf.gz)
#echo "Starting snpSift with dbSNP, ExAC, Cosmic CodingMuts, and Clinvar on sample (basename): $samplebase"

#SnpSift Annotate /data/share/cce/reference/dbSNP/All_20170710.vcf.gz  $sample  > ${samplebase}.dbSNP.vcf
#bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                 ${samplebase}.dbSNP.vcf
#rm                                                                               ${samplebase}.dbSNP.vcf

	#SnpSift Annotate /DATA/share/cce/reference/ExAC/ExAC.r1.sites.vep.vcf.gz ${samplebase}.dbSNP.vcf.gz  > ${samplebase}.dbSNP.ExAC.vcf
	#bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh ${samplebase}.dbSNP.ExAC.vcf
	#rm ${samplebase}.dbSNP.ExAC.vcf

#SnpSift dbnsfp -v -f ExAC_AF -db /DATA/share/cce/reference/dbNSFP/dbNSFP3.5a_hg19.txt.gz ${samplebase}.dbSNP.vcf.gz > ${samplebase}.dbSNP.ExAC.vcf
#bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                      ${samplebase}.dbSNP.ExAC.vcf
#rm                                                                                                                    ${samplebase}.dbSNP.ExAC.vcf


#SnpSift Annotate /DATA/share/cce/reference/cosmic/CosmicCodingMuts.vcf.gz ${samplebase}.dbSNP.ExAC.vcf.gz  >          ${samplebase}.dbSNP.ExAC.cosmic.vcf
#bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                      ${samplebase}.dbSNP.ExAC.cosmic.vcf
#rm                                                                                                                    ${samplebase}.dbSNP.ExAC.cosmic.vcf


#SnpSift Annotate /DATA/share/cce/reference/clinvar/clinvar_20180401.vcf.gz  ${samplebase}.dbSNP.ExAC.cosmic.vcf.gz  > ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf
#bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                      ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf
#rm                                                                                                                    ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf

#echo "Starting snpEff on sample (basename): $samplebase"
#snpEff  -s ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.html GRCh37.75 ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf.gz  > ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.vcf
#bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                        ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.vcf
#rm ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.vcf


#done
