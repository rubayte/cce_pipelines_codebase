# README
# This script does:
# run Strelka, merge snv+indel, filter for target region bed

# Input paramters, required:
cores=$1
tumorbam=$2
normalbam=$3
referencefasta=$4
outpath=$5
bedfilter=$6

## create outpaths
OUT=$outpath/strelka

if [ ! -d "$OUT" ]; then
	mkdir $OUT
fi

## config strelka
python /scif/apps/strelka/strelka/bin/configureStrelkaSomaticWorkflow.py \
	--tumorBam $tumorbam \
	--normalBam $normalbam \
	--referenceFasta $referencefasta \
	--exome \
	--indelCandidates $outpath/manta/results/variants/candidateSmallIndels.vcf.gz \
	--runDir=$OUT

## run strelka
python $OUT/runWorkflow.py -m local -j $cores


## vcf merge
java -jar /scif/apps/rtgtools/rtg-tools/dist/rtg-tools-3.10.1-4d58ead/RTG.jar vcfmerge \
	-o $OUT/results/variants/somatic.snvindel.vcf.gz \
	--force-merge-all \
	$OUT/results/variants/somatic.snvs.vcf.gz \
	$OUT/results/variants/somatic.indels.vcf.gz

## filter vcf
java -jar /scif/apps/rtgtools/rtg-tools/dist/rtg-tools-3.10.1-4d58ead/RTG.jar vcffilter \
	--bed-regions=$bedfilter \
	--input=$OUT/results/variants/somatic.snvindel.vcf.gz \
	--output=$OUT/results/variants/somatic.snvindel.targetbed.vcf.gz


