use warnings;
use strict;
use lib qw(..);
use JSON qw( );

if (@ARGV != 7)
{
        print "usage: $0 <MVL1.vcf> <MVL1.indels.tsv> <MVL2.vcf> <MVL2.indels.tsv> <input VCF> <output VCF> <bedfile>\n";
        exit(0);
}


my %oncokb = ();
my %mvl = ();
my %mvlvcf = ();
my %oncokbvcf = ();
my %oncokb_tm = ();
my %bedregions = ();
my @snpeff_tokens = ('stop_gained', 'stop_lost', 'stop_retained_variant', 'frameshift_variant&stop_gained&missense_variant', 'frameshift_variant&stop_gained', 'stop_gained&splice_region_variant');



my $containerVersion = "";
my $sinconfigfile = "/.singularity.d/labels.json";

my $json_text = do {
   open(my $json_fh, "<:encoding(UTF-8)", $sinconfigfile)
      or die("Can't open \$filename\": $!\n");
   local $/;
   <$json_fh>
};
my $json = JSON->new;
my $data = $json->decode($json_text);
$containerVersion = $data->{'VERSION'};

sub changeLetterCode{
	
	my $l = shift;
	my $nl = "";
	if ($l eq "Phe"){
		$nl = "F";
	}elsif ($l eq "Leu"){
		$nl = "L";
	}elsif ($l eq "Met"){
                $nl = "M";
        }elsif ($l eq "Val"){
                $nl = "V";
        }elsif ($l eq "Ser"){
                $nl = "S";
        }elsif ($l eq "Pro"){
                $nl = "P";
        }elsif ($l eq "Thr"){
                $nl = "T";
        }elsif ($l eq "Ala"){
                $nl = "A";
        }elsif ($l eq "Tyr"){
                $nl = "Y";
        }elsif ($l eq "His"){
                $nl = "H";
        }elsif ($l eq "Gln"){
                $nl = "Q";
        }elsif ($l eq "Asn"){
                $nl = "N";
        }elsif ($l eq "Lys"){
                $nl = "K";
        }elsif ($l eq "Asp"){
                $nl = "D";
        }elsif ($l eq "Glu"){
                $nl = "E";
        }elsif ($l eq "Cys"){
                $nl = "C";
        }elsif ($l eq "Trp"){
                $nl = "W";
        }elsif ($l eq "Arg"){
                $nl = "R";
        }elsif ($l eq "Ser"){
                $nl = "S";
        }elsif ($l eq "Gly"){
                $nl = "G";
        }elsif ($l eq "Ile"){
                $nl = "I";
        }else{
		$nl = "";
	}
	
	return $nl;
}

sub transformP{

	my $snpEffPc = shift;
	my $spc = substr($snpEffPc,2);
	my $fl = substr($spc,0,3);
	my $sl = substr($spc,-3,);
	my $pos = substr($spc,3,-3);
	my $newPc = changeLetterCode($fl).$pos.changeLetterCode($sl);
	return $newPc
}

## MVL1 snps vcf file
open (MVL1VCF, $ARGV[0]) or die "could not open MVL1 vcf file\n";

while (<MVL1VCF>){
	chomp $_;
	if ($_ !~ m/^#/){
		my @dt = split("\t", $_);
		my $key = $dt[0].";".$dt[1].";".$dt[3].";".$dt[4];
		my $value = "";
		my @infos = split(";", $dt[7]);
		foreach my $item (@infos){
			if ($item =~ /^CLASSIFICATION=/){
				$value = substr($item, 15);
			}
		}
		$mvlvcf { $key } = $value;
	}
}

close(MVL1VCF);

## MVL2 snps vcf file
open (MVL2VCF, $ARGV[2]) or die "could not open MVL2 vcf file\n";

while (<MVL2VCF>){
        chomp $_;
        if ($_ !~ m/^#/){
                my @dt = split("\t", $_);
                my $key = $dt[0].";".$dt[1].";".$dt[3].";".$dt[4];
                my $value = "";
                my @infos = split(";", $dt[7]);
                foreach my $item (@infos){
                        if ($item =~ /^CLASSIFICATION=/){
                                $value = substr($item, 15);
                        }
                }
                $oncokbvcf { $key } = $value;
        }
}

close(MVL2VCF);

## MVL1 indels file
open (MVL, $ARGV[1]) or die "could not open MVL1 indels file\n";

while (<MVL>){
        chomp $_;
        my @dt = split("\t", $_);

        if ($dt[0] !~ m/^Position/){

                my ($chr,$pos) = split(":", $dt[0]);
                if ($pos =~ m/\-/){
                        my @tpos = split("\-", $pos);
                        $pos = $tpos[0];
                }
                ## correct the position for deletion calls
                if ($dt[4] eq "deletion"){
                        $pos = $pos - 1;
                }
                ## check on gene, chr and position
                my $key = $chr.";".$pos;
                #my $value = $dt[1]."#".$dt[8]."#".$dt[3]."#".$dt[9]."(".$dt[7].")";
                my $value = $dt[1]."#".$dt[8]."#".$dt[3]."#".$dt[9];
		$mvl{ $key } = $value;
        }

}

close(MVL);

## MVL2 indels file
open (ONCOKB, $ARGV[3]) or die "could not open MVL2 indels file\n";

while (<ONCOKB>){
	chomp $_;
	my @dt = split("\t", $_);
	
	if ($dt[0] !~ m/^Isoform/){
	
		## check on gene, transcript, protein change
		if ($dt[5] eq "Truncating Mutations" or $dt[5] =~ /^Promoter/){
			my $key = $dt[3].";".$dt[0];
			my $value = $dt[6];
			$oncokb_tm{ $key } = $value;
		}else{
			my $key = $dt[3].";".$dt[0].";".$dt[5];
			#my $value = $dt[6]."(".$dt[7].")";
			my $value = $dt[6];
			$oncokb{ $key } = $value;
		}
	}

}

close(ONCOKB);


## read bed file for target regions
open (BED, $ARGV[6]) or die "could not open bed regions file\n";

while (<BED>){
        chomp $_;
        my @dt = split("\t", $_);
	for (my $i = $dt[1]; $i <= $dt[2]; $i++){
		my $key = $dt[0].";".$i;
		$bedregions{ $key } = 1;
	}
}

close(BED);

## input VCF and output VCF
open (NVCF, ">".$ARGV[5]) or die "could not open new file $ARGV[5] file \n";
open (VCF, $ARGV[4]) or die "could not open input VCF file\n";

while (<VCF>){
	
	chomp $_;
	if ($_ =~ m/^#/){
		## header
		if ($_ =~ /^##fileformat/){
			print NVCF $_."\n";
			print NVCF "##cce_container_version=$containerVersion\n";
			print NVCF "##cce_mvl1_version=20200220\n";
			print NVCF "##cce_mvl2_version=20190114\n";
			print NVCF "##FILTER=<ID=MVL1,Description=\"MVL1 tagged\">\n";
			print NVCF "##FILTER=<ID=MVL2,Description=\"MVL2 tagged\">\n";
			print NVCF "##FILTER=<ID=EXT,Description=\"Position is in extended bed region\">\n";
			print NVCF "##INFO=<ID=MVL1,Number=1,Type=String,Description=\"MVL1 Classification\">\n";
			print NVCF "##INFO=<ID=MVL2,Number=1,Type=String,Description=\"MVL2 Classification\">\n";
		}else{
			print NVCF $_."\n";
		}
	}else{
		## data
		my $ann = "";
		my @dt = split("\t", $_);
		my @infos = split(";", $dt[7]);
		my $filterLabel = "";
                my $filterLabelMVL = "";
		
		foreach my $item (@infos){
                	if ($item =~ m/^ANN=/){
                		$ann = substr($item,4);
                        }
                }
		my @anns = split(",", $ann);
		
		## check position in extended bed region
		if ($dt[0] =~ /^chr/ or $dt[0] =~ /^Chr/){
			$dt[0] = substr($dt[0], 3);
		}
		my $bkey = $dt[0].";".$dt[1];
		if (not(exists $bedregions{ $bkey })){
			$dt[6] = $dt[6].";"."EXT";
		}
	
		## handle snps
		if (length($dt[3]) == 1 and length($dt[4]) == 1 and ($dt[3] ne "." or $dt[4] ne ".")){
			if ($dt[0] =~ /^chr/ or $dt[0] =~ /^Chr/){
				$dt[0] = substr($dt[0], 3);
			}
			if (exists $mvlvcf{ $dt[0].";".$dt[1].";".$dt[3].";".$dt[4] }){
				$filterLabelMVL = $filterLabelMVL.",".$mvlvcf{ $dt[0].";".$dt[1].";".$dt[3].";".$dt[4] };
			}
			if (exists $oncokbvcf{ $dt[0].";".$dt[1].";".$dt[3].";".$dt[4] }){
                                $filterLabel = $filterLabel.",".$oncokbvcf{ $dt[0].";".$dt[1].";".$dt[3].";".$dt[4] };
                        }
			## handle onck kb truncating and promoter mutations
			foreach my $as (@anns){
                                my @annd = split(/\|/, $as);
                        	if ( $annd[3] ne "" and $annd[6] ne ""){
                                        my $nkey = $annd[3].";".$annd[6];
                                        if ( (grep { $_ eq $annd[1] } @snpeff_tokens) or ($annd[1] =~ m/frameshift_variant/) ){
                                                if (exists $oncokb_tm{ $nkey }){
                                                        $filterLabel = $filterLabel.",".$oncokb_tm{ $nkey };
                                                }
                                        }
                                }
			}
		}else{
			## handle indels
			foreach my $as (@anns){
				my @annd = split(/\|/, $as);
			
				## onco kb
				if ( $annd[3] ne "" and $annd[6] ne "" and defined $annd[10] and $annd[10] ne ""){
					my $pchange = transformP($annd[10]);
					my $nkey = $annd[3].";".$annd[6].";".$pchange;
					if (exists $oncokb{ $nkey }){
						$filterLabel = $filterLabel.",".$oncokb{ $nkey };
					}
				}
				## handle onck kb truncating mutations
				if ( $annd[3] ne "" and $annd[6] ne ""){
					my $nkey = $annd[3].";".$annd[6];
					if ( (grep { $_ eq $annd[1] } @snpeff_tokens) or ($annd[1] =~ m/frameshift_variant/) ){
						#if ($annd[1] in @snpeff_tokens){
						if (exists $oncokb_tm{ $nkey }){
                                                	$filterLabel = $filterLabel.",".$oncokb_tm{ $nkey };
                                        	}
					}
				}
				
				## nki mvl
				my $keyMVL = $dt[0].";".$dt[1];
				if (exists $mvl{ $keyMVL }){
					## check on p change
					my ($gene, $pc, $cc, $class) = split("#", $mvl{ $keyMVL });
					if ($gene ne "" and $pc ne "" and $cc ne ""){
						if ($gene eq $annd[3] and defined $annd[10] and $pc eq $annd[10]){
							$filterLabelMVL = $filterLabelMVL.",".$class;
						}elsif ($gene eq $annd[3] and $cc eq $annd[9]){
							$filterLabelMVL = $filterLabelMVL.",".$class;
						}
						else{
							## do nothing
						}
					}
                        	}

			}
		}
		
		## clean up the initial commma
		if ($filterLabel =~ m/^\,/){
			$filterLabel = substr($filterLabel,1);
		}
		if ($filterLabelMVL =~ m/^\,/){
                        $filterLabelMVL = substr($filterLabelMVL,1);
                }
		
		## add the labels to info column
		if ($filterLabelMVL ne ""){
			push (@infos, "MVL1=".$filterLabelMVL);
			$dt[6] = $dt[6].";"."MVL1";
		}
		if ($filterLabel ne ""){
			push (@infos, "MVL2=".$filterLabel);
			$dt[6] = $dt[6].";"."MVL2";
		}
		
		$dt[7] = join(";", @infos);
		
		print NVCF join("\t", @dt)."\n";
	}
}

close(VCF);
close(NVCF);
