
#SnpSift.jar Annotate [options] database.vcf file.vcf > newFile.vcf

#bedfile=$1


for sample in *.vcf.gz
do

samplebase=$(basename "$sample" .vcf.gz)

echo "Starting snpSift with dbSNP, ExAC, Cosmic CodingMuts, and Clinvar on sample (basename): $samplebase"

SnpSift Annotate /data/share/cce/reference/dbSNP/All_20170710.vcf.gz  $sample  > ${samplebase}.dbSNP.vcf
bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                 ${samplebase}.dbSNP.vcf
rm                                                                               ${samplebase}.dbSNP.vcf

#SnpSift Annotate /DATA/share/cce/reference/ExAC/ExAC.r1.sites.vep.vcf.gz ${samplebase}.dbSNP.vcf.gz  > ${samplebase}.dbSNP.ExAC.vcf
#bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh ${samplebase}.dbSNP.ExAC.vcf
#rm ${samplebase}.dbSNP.ExAC.vcf

SnpSift dbnsfp -v -f ExAC_AF -db /DATA/share/cce/reference/dbNSFP/dbNSFP3.5a_hg19.txt.gz ${samplebase}.dbSNP.vcf.gz > ${samplebase}.dbSNP.ExAC.vcf
bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                      ${samplebase}.dbSNP.ExAC.vcf
rm                                                                                                                    ${samplebase}.dbSNP.ExAC.vcf


SnpSift Annotate /DATA/share/cce/reference/cosmic/CosmicCodingMuts.vcf.gz ${samplebase}.dbSNP.ExAC.vcf.gz  >          ${samplebase}.dbSNP.ExAC.cosmic.vcf
bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                      ${samplebase}.dbSNP.ExAC.cosmic.vcf
rm                                                                                                                    ${samplebase}.dbSNP.ExAC.cosmic.vcf


SnpSift Annotate /DATA/share/cce/reference/clinvar/clinvar_20180401.vcf.gz  ${samplebase}.dbSNP.ExAC.cosmic.vcf.gz  > ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf
bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                      ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf
rm                                                                                                                    ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf

echo "Starting snpEff on sample (basename): $samplebase"
snpEff  -s ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.html GRCh37.75 ${samplebase}.dbSNP.ExAC.cosmic.clinvar.vcf.gz  > ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.vcf
bash /DATA/share/cce/somavarcee_20180222_1/create_vcfgz_tabix.sh                                                        ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.vcf
#rm ${samplebase}.dbSNP.ExAC.cosmic.clinvar.eff.vcf


done
