#README
# This script does:
# Align read pairs to ref genome
# QC BAMs with fastqc and multiqc


# Input parameter, required:
cores="$1"
fastqR1=$2 
fastqR2=$3
outpath=$4
sample=$5
pathToRef=$6

## create outpaths
OUT=$outpath/bams
OUTQC=$outpath/bamQC

if [ ! -d "$OUT" ]; then
	mkdir $OUT
fi
if [ ! -d "$OUTQC" ]; then
	mkdir $OUTQC
fi


## create bam file
bwa mem -t $cores $pathToRef <(pigz -d -p $cores -c $fastqR1) <(pigz -d -p $cores -c $fastqR2) | samtools view -Su - | samtools sort -l 0 -@ $cores -o $OUT/$sample.sorted.bam -
picard MarkDuplicates COMPRESSION_LEVEL=9 I=$OUT/$sample.sorted.bam O=$OUT/$sample.sorted.markdup.bam M=$OUTQC/$sample.sorted.marked_dup_metrics.txt
# rm $OUT/$sample.sorted.bam
samtools index -@ $cores -b $OUT/$sample.sorted.markdup.bam

## bam QC
fastqc --threads $cores --format bam_mapped --outdir $OUTQC $OUT/*.bam
multiqc --outdir $OUTQC $OUTQC


## ************************************************************************************************************************************
# Input parameter, required:
# cores="$1"
# suffixR1="$2"
# suffixR2="$3"
# pathToRef="$4" # e.g. $HOME/genomes/hs37d5_phiX174.fa 

# List of used tools, their versions being logged:
# echo "BWA on all readpairs, samtools sort and Picard markduplicates" >> versions.log
# fastqc --version >> versions.log
# multiqc --version >> versions.log
# echo -n "bwa mem " >> versions.log
# bwa 2>&1 |grep Version >> versions.log
# samtools --version|grep 'samtools\|Using' >> versions.log
# echo -n "Picard Tools " >> versions.log
# picard MarkDuplicates --version >> versions.log


# for file in *${suffixR1}
# do




#   inputfqgz_1=$file
#   inputfqgz_2=${file/%${suffixR1}/${suffixR2}}
#   prefix=${file/%${suffixR1}/}                      #  % means that suffixes are matched, # would mean prefixes
#   bwa mem -t $cores $pathToRef <(pigz -d -p $cores -c $inputfqgz_1) <(pigz -d -p $cores -c $inputfqgz_2) | samtools view -Su - | samtools sort -l 0 -@ $cores -o $prefix.sorted.bam -
#   picard MarkDuplicates COMPRESSION_LEVEL=9 I=$prefix.sorted.bam O=$prefix.markdup.bam M=$prefix.marked_dup_metrics.txt
#   rm $prefix.sorted.bam
#   samtools index -@ $cores -b $prefix.markdup.bam 
#
#done

#mkdir -p fastq 
#mv *fq.gz fastq

#mkdir bam
# mv *bam bam     # do that later
# mv *bam.bai bam  # do that late

#mkdir bam-QC
#mv *marked_dup_metrics.txt bam-QC


#fastqc --threads $cores --format bam_mapped --outdir bam-QC *.bam
#multiqc --outdir ./bam-QC ./bam-QC

