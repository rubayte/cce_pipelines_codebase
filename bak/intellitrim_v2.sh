
# Input parameter, required:
cores="$1"
suffixR1="$2"
suffixR2="$3"


for file in *${suffixR1}
do
   inputfqgz_1=$file
   inputfqgz_2=${file/%${suffixR1}/${suffixR2}}
   echo $inputfqgz_1
   echo $inputfqgz_2

   stem1=${inputfqgz_1/%.fq.gz/} # ends with .R1
   stem2=${inputfqgz_2/%.fq.gz/} # ends with .R2
   echo $stem1
   echo $stem2
   OVSEQ1="$(unzip -c ./trim1-QC/${stem1}_fastqc.zip ${stem1}_fastqc/fastqc_data.txt | awk 'f{print;f=0} /#Sequence\s+Count\s+Percentage\s+Possible\s+Source/{f=1}' |  awk '{print $1;}')" # first overrepresented sequence in R1
   OVSEQ2="$(unzip -c ./trim1-QC/${stem2}_fastqc.zip ${stem2}_fastqc/fastqc_data.txt | awk 'f{print;f=0} /#Sequence\s+Count\s+Percentage\s+Possible\s+Source/{f=1}' |  awk '{print $1;}')" # fírst overrepresented sequence in R2
   min1adapt=GATCGGAAGAGCACACGTCT
   min1adaptreverse=GATCGGAAGAGCGTCGTGTA
   if [[ $OVSEQ1 =~  ^$min1adapt  ]] || [[ $OVSEQ2 =~  ^$min1adaptreverse ]]; then
   echo "There is a remaining adapter $min1adapt in $stem1 or $min1adaptreverse in $stem2"
   echo "... going to retrim these adapters off the forward and reverse read" 
   newprefix=${stem1/%.R1/}
   SeqPurge -threads $cores -a1 $min1adapt -a2 $min1adaptreverse -in1 $inputfqgz_1 -in2 $inputfqgz_2 -out1 $newprefix.trim.R1.fq.gz -out2 $newprefix.trim.R2.fq.gz
   mv  $inputfqgz_1 fastq
   mv  $inputfqgz_2 fastq
   numberOfTrims=$(grep -o "trim" <<< "$newprefix.trim.R1.fq.gz" | wc -l)
   mkdir -p trim${numberOfTrims}-QC

   fastqc --threads $cores --outdir ./trim${numberOfTrims}-QC ./$newprefix.trim.R*.fq.gz
   multiqc --interactive   --outdir  ./trim${numberOfTrims}-QC  ./trim${numberOfTrims}-QC/*
   fi
done


