# README
# This script does:
# QC with fastqc and multiqc
# trimming with SeqPurge using defaults
# QC with fastqc and multiqc


# Input parameter, required:
cores="$1"
declare -a fastqs=($2 $3 $4 $5)
outpath=$6
sample=$7

## create outpaths
OUT1=$outpath/QC-round-1
OUT2=$outpath/QC-round-2

if [ ! -d "$OUT1" ]; then
        mkdir $OUT1
fi
if [ ! -d "$OUT2" ]; then
        mkdir $OUT2
fi

# first round QC
mkdir $outpath/QC-round-1
for i in "${fastqs[@]}"
do
	fastqc --threads $cores --outdir $OUT1 $i
done
multiqc --interactive --outdir $OUT1  $OUT1/

# trim all fastq files, forward and reverse reads
SeqPurge -threads $cores -in1 $2 -in2 $3 -out1 $outpath/$sample.tumor.trim.R1.fq.gz -out2 $outpath/$sample.tumor.trim.R2.fq.gz
SeqPurge -threads $cores -in1 $4 -in2 $5 -out1 $outpath/$sample.normal.trim.R1.fq.gz -out2 $outpath/$sample.normal.trim.R2.fq.gz

# second round QC
fastqc --threads $cores --outdir $OUT2 $outpath/*trim*.fq.gz
multiqc --interactive --outdir $OUT2 $OUT2/

