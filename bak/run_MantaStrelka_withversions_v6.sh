# README
# This script does:
# run Manta, Strelka, merge snv+indel, filter for target region bed

# List of used tools, their versions being logged:
echo -n "Manta " >> versions.log
configManta.py --version >> versions.log
echo -n "Stelka " >> versions.log
configureStrelkaSomaticWorkflow.py --version >> versions.log
echo -n "RTG Tools " >> versions.log
rtg --version |grep Version >> versions.log


# Input paramters, required:
cores="$1"
tumorsubstring="$2"
normalsubstring="$3"
PatIDformat="$4"
referencefasta="$5" # formated with rtg tools; input the name of the dir with all files (sdf and others)
bedfilter="$6" #  the capture targets bed file of your target enrichment kit, which the vendor recommends for variant calling

for file in *.markdup.bam
do

if [[ $file == *"$normalsubstring"* ]];
then
   echo "The normalstring is in $file"
   continue
else
if [[ $file =~ ($PatIDformat) ]]; then
    patid=${BASH_REMATCH[1]}
    echo "this will be the tumorbam:"
    echo $file
    echo "this is the patid:"
    echo $patid
else
    echo "unable to parse string $file and to determine matching normal "
    continue
fi

 tumorbam=$file
 normalfiles=( $normalsubstring*$patid*bam )
 normalbam=${normalfiles[0]}
 echo "this will be the normalbam:"
 echo $normalbam
 # in early versions this was used: normalbam=${file/$tumorsubstring/$normalsubstring}
 filenamewithoutdotmarkdupdotbam=${file/%.markdup.bam/}
 echo $filenamewithoutdotmarkdupdotbam
 # in early versions this was used: sample=${filenamewithoutdotmarkdupdotbam/$tumorsubstring/}
 sample=$patid
 echo "Starting Manta and Strelka on Sample:"
echo $sample
configManta.py \
--tumorBam $tumorbam \
--normalBam $normalbam \
--referenceFasta $referencefasta \
--exome \
--runDir="Manta_$sample"

./Manta_$sample/runWorkflow.py -m local -j $cores

configureStrelkaSomaticWorkflow.py \
--tumorBam $tumorbam \
--normalBam $normalbam \
--referenceFasta $referencefasta \
--exome \
--indelCandidates ./Manta_$sample/results/variants/candidateSmallIndels.vcf.gz \
--runDir="StrelkaSomatic_$sample"


./StrelkaSomatic_$sample/runWorkflow.py -m local -j $cores


rtg vcfmerge \
-o ./StrelkaSomatic_$sample/results/variants/somatic.snvindel.vcf.gz \
--force-merge-all \
./StrelkaSomatic_$sample/results/variants/somatic.snvs.vcf.gz \
./StrelkaSomatic_$sample/results/variants/somatic.indels.vcf.gz

rtg vcffilter \
--bed-regions=$bedfilter \
--input=./StrelkaSomatic_$sample/results/variants/somatic.snvindel.vcf.gz \
--output=./StrelkaSomatic_$sample/results/variants/somatic.snvindel.targetbed.vcf.gz

mkdir -p vcf-Strelka
cp ./StrelkaSomatic_${sample}/results/variants/somatic.snvindel.targetbed.vcf.gz  ./vcf-Strelka/${sample}_Strelka_somatic.snvindel.targetbed.vcf.gz

fi
done
