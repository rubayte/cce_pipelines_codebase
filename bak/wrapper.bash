
R1fastqgzSuffix=$1   # e.g. _R1.fq.gz
R2fastqgzSuffix=$2   # e.g. _R2.fq.gz
tumorPrefix=$3       # e.g. tumor_
normalPrefix=$4      # e.g. normal_
PatIDformat=$5       # e.g. pt.[0-9]{2}
pathToRef=$6         # e.g. /DATA/share/cce/reference/hs37d5_phiX174.fa    # http://googlegenomics.readthedocs.io/en/latest/use_cases/discover_public_data/reference_genomes.html
pathToBED=$7         # e.g. /DATA/share/cce/target-BEDs/CCC/CMDL_130217_capture_targets_noChr.bed
cores=$8             # e.g. 32
scriptDir=$9         # e.g. /DATA/share/cce/somavarcee_20180115_1/

bash $scriptDir/QC-trim1-QC_v2.sh $cores $R1fastqgzSuffix $R2fastqgzSuffix
bash $scriptDir/intellitrim_v2.sh $cores .trim.R1.fq.gz .trim.R2.fq.gz
bash $scriptDir/bwa-onall-fqgz-QC_v3.sh $cores .trim.R1.fq.gz .trim.R2.fq.gz $pathToRef
bash $scriptDir/run_MantaStrelka_withversions_v6.sh $cores $tumorPrefix $normalPrefix $PatIDformat $pathToRef $pathToBED


