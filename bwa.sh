#README
# This script does:
# Align read pairs to ref genome
# QC BAMs with fastqc and multiqc


# Input parameter, required:
cores="$1"
fastqR1=$2 
fastqR2=$3
outpath=$4
sample=$5
pathToRef=$6
bed=$7

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

## create outpaths
OUT=$outpath/bams
OUTQC=$outpath/bamQC

if [ ! -d "$OUT" ]; then
	mkdir $OUT
fi
if [ ! -d "$OUTQC" ]; then
	mkdir $OUTQC
fi


## create bam file
bwa mem -t $cores $pathToRef <(pigz -d -p $cores -c $fastqR1) <(pigz -d -p $cores -c $fastqR2) | samtools view -Su - | samtools sort -l 0 -@ $cores -o $OUT/$sample.sorted.bam -
java -jar /scif/apps/picard/picard.jar MarkDuplicates COMPRESSION_LEVEL=9 I=$OUT/$sample.sorted.bam O=$OUT/$sample.sorted.markdup.bam M=$OUTQC/$sample.sorted.marked_dup_metrics.txt
java -jar /scif/apps/picard/picard.jar AddOrReplaceReadGroups I=$OUT/$sample.sorted.markdup.bam O=$OUT/$sample.sorted.markdup.rg.bam RGSM=$sample RGLB=lib1 RGPL=illumina RGPU=unit1 RGID=$sample
rm $OUT/$sample.sorted.bam
rm $OUT/$sample.sorted.markdup.bam
mv $OUT/$sample.sorted.markdup.rg.bam $OUT/$sample.sorted.markdup.bam
samtools index -@ $cores -b $OUT/$sample.sorted.markdup.bam

## bam QC
fastqc --threads $cores --format bam_mapped --outdir $OUTQC $OUT/*.bam
multiqc --outdir $OUTQC $OUTQC
samtools view -uF 0x400 $OUT/$sample.sorted.markdup.bam | /scif/apps/bedtools/bedtools2/bin/bedtools coverage -a $bed -b - -mean | cut -f 5 >>  $OUTQC/$sample.mean.txt
samtools view -uF 0x400 $OUT/$sample.sorted.markdup.bam | /scif/apps/bedtools/bedtools2/bin/bedtools coverage -a $bed -b - -d | cut -f 6 >>  $OUTQC/$sample.perbase.txt

