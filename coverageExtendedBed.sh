#README
# calculate mean and perbase coverage on the extended bed file regions 


# Input parameter, required:
bam=$1
outpath=$2
sample=$3
bed=$4

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

## create outpaths
OUTQC=$outpath

if [ ! -d "$OUTQC" ]; then
	mkdir $OUTQC
fi


## bam coverage
samtools view -uF 0x400 $bam | /scif/apps/bedtools/bedtools2/bin/bedtools coverage -a $bed -b - -mean | cut -f 5 >>  $OUTQC/$sample.mean.extended.txt
samtools view -uF 0x400 $bam | /scif/apps/bedtools/bedtools2/bin/bedtools coverage -a $bed -b - -d | cut -f 6 >>  $OUTQC/$sample.perbase.extended.txt

