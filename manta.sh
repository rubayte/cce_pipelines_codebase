# README
# This script does:
# run Manta tool


# Input paramters, required:
cores=$1
tumorbam=$2
normalbam=$3
referencefasta=$4
outpath=$5

## create outpaths
OUT=$outpath/manta

if [ ! -d "$OUT" ]; then
	mkdir $OUT
fi

## config manta
python /scif/apps/manta/manta/bin/configManta.py \
	--tumorBam $tumorbam \
	--normalBam $normalbam \
	--referenceFasta $referencefasta \
	--exome \
	--runDir=$OUT

## run manta
python $OUT/runWorkflow.py -m local -j $cores


