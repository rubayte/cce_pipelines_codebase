# README
# run cnvkit

# Input paramters, required:
tumorbam=$1
cores=$2
outpath=$3
sample=$4
normalreference=$5
ballelevcf=$6
tcp=$7

## create outpaths
OUT=$outpath/cnvkit

if [ ! -d "$OUT" ]; then
	mkdir $OUT	
fi
cd $OUT

## run cnvkit batch on the tumor bam file
/usr/bin/python3.5 /scif/apps/cnvkit/cnvkit/cnvkit.py batch \
	$tumorbam \
        -r $normalreference \
        -p $cores\

## make calls
if [ $tcp != "NA" ];then
	/usr/bin/python3.5 /scif/apps/cnvkit/cnvkit/cnvkit.py call \
		$sample.tumor.sorted.markdup.cns \
		-y \
		--purity $tcp \
		-o $sample.tumor.sorted.markdup.call.purity.cns
fi

## cnvkit plots
if [ -e $ballelevcf ]; then
	/usr/bin/python3.5 /scif/apps/cnvkit/cnvkit/cnvkit.py scatter \
		$sample.tumor.sorted.markdup.cnr \
		-s $sample.tumor.sorted.markdup.cns \
		-v $ballelevcf \
		-i $sample.tumor \
		-o $sample.scatter.pdf
else
	/usr/bin/python3.5 /scif/apps/cnvkit/cnvkit/cnvkit.py scatter \
	        $sample.tumor.sorted.markdup.cnr \
        	-s $sample.tumor.sorted.markdup.cns \
                -o $sample.scatter.pdf
fi
/usr/bin/python3.5 /scif/apps/cnvkit/cnvkit/cnvkit.py diagram \
       	$sample.tumor.sorted.markdup.cnr \
       	-s $sample.tumor.sorted.markdup.cns \
       	-o $sample.diagram.pdf

cd -

# /usr/bin/python3.5 /scif/apps/cnvkit/cnvkit/cnvkit.py batch \
#	$bams/*Tumor.bam \
#	--normal $bams/*Normal.bam \
#	-p 0 \
#    	--targets $bedfile \ 
#	--fasta $reffasta \
#    	--access $accessbed \
#    	# --annotate $refflat \
#    	--output-reference $refcnn \ 
#	--output-dir $outdir_rf \
#    	--scatter \
#	--diagram


