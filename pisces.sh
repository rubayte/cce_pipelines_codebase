# README
# This script does:
# run pisces tool


# Input paramters, required:
bedfile=$1
pathToGenomeSizeXML=$2
markdupbam=$3
outpath=$4

## create outpaths
OUT=$outpath/pisces

if [ ! -d "$OUT" ]; then
	mkdir $OUT
fi

## run pisces
dotnet /scif/apps/pisces/Pisces_5.2.10.49/Pisces.dll --callmnvs false --threadbychr true --gvcf false -g $pathToGenomeSizeXML -b $markdupbam -i $bedfile --outfolder $OUT



