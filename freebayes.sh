# README
# This script does:
# run freebayes tool


# Input paramters, required:
bam=$1
referencefasta=$2
bedfile=$3
outvcf=$4
outpath=$5

## create outpaths
OUT=$outpath/freebayes

if [ ! -d "$OUT" ]; then
	mkdir $OUT
fi

## run freebayes
/scif/apps/freebayes/freebayes/bin/freebayes \
	--f $referencefasta \
	--targets $bedfile \
	-F 0.025 \
	--pooled-continuous $bam > $OUT/$outvcf


